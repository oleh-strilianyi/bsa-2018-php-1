<?php
/**
 * Created by PhpStorm.
 * User: Oleh Strilianyi
 * Date: 10.05.2018
 * Time: 18:12
 */

namespace Cryptocurrency\Task1;

class Dogecoin implements Currency
{
    private $name;
    private $logo;
    private $price;

    public function __construct(float $price)
    {
        $this->price = $price;
        $this->name = 'Dogecoin';
        $this->logo = 'https://s2.coinmarketcap.com/static/img/coins/32x32/74.png';
    }

    public function getDailyPrice(): float
    {
        return $this->price;
    }

    public function getLogoUrl(): string
    {
        return $this->logo;
    }

    public function getName(): string
    {
        return $this->name;
    }
}