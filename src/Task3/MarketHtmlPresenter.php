<?php declare(strict_types=1);

namespace Cryptocurrency\Task3;

use Cryptocurrency\Task1\CoinMarket;

class MarketHtmlPresenter
{
    public function present(CoinMarket $market): string
    {
        $htmlString = '<ul>';
        foreach ($market->getCurrencies() as $item){
            $htmlString .= '<li><img src="';
            $htmlString .= $item->getLogoUrl() . '">';
            $htmlString .= $item->getName() . ': ';
            $htmlString .= $item->getDailyPrice() . '</li>';
        }
        
        $htmlString .= '<ul>';
        return $htmlString;
    }
}