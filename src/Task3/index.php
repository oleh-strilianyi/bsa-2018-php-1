<?php

require __DIR__ . '/../../vendor/autoload.php';

use Cryptocurrency\Task1\CoinMarket;
use Cryptocurrency\Task3\MarketHtmlPresenter;
use Cryptocurrency\Task1\{Bitcoin, Ethereum, Dogecoin};

$market = new CoinMarket();
$marketPresenter = new MarketHtmlPresenter();
$bitcoin = new Bitcoin(9361.44);
$dogecoin = new Dogecoin(0.0052);
$ethereum = new Ethereum(763.20);
$market->addCurrency($bitcoin);
$market->addCurrency($dogecoin);
$market->addCurrency($ethereum);
$presentation = $marketPresenter->present($market);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
</head>
<body>
<?php echo $presentation ?>
</body>
</html>